Репозиторий для 1 задания в марафоне для Android-разработчиков от VibeLab

В файле [exercises](https://gitlab.com/NikitaEfimov0/introduction/-/blob/main/exercises) хранится код для каждого из заданий с Kotlin Koans

Файл [task1.png](https://gitlab.com/NikitaEfimov0/introduction/-/blob/main/task1.png) является скриншотом, на котором показан успешный результат прохождения заданий.

